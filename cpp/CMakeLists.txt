cmake_minimum_required(VERSION 3.20.0)

set(DCMAKE_EXPORT_COMPILE_COMMANDS 1)
set(CMAKE_CXX_STANDARD 20)

project(cpp-project VERSION 0.0.1)

add_executable(main
    "${PROJECT_SOURCE_DIR}/src/main.cpp"
)