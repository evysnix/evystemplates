{
  description = "Simple rust project";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    moz-overlay.url = "github:mozilla/nixpkgs-mozilla";
  };

  outputs =
    inputs@{ self
    , nixpkgs
    , utils
    , moz-overlay
    , ...
    }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ moz-overlay.overlay ];
      };

      rustToolchain = pkgs.latest.rustChannels.nightly.rust.override (old: {
        extensions = ["rust-src" "rust-analysis"];
      });

      buildInputs = with pkgs; [ ]; # dependencies here
    in
    {
      devShell = with pkgs; mkShell {
        buildInputs = buildInputs ++ [
          rustToolchain
        ];
        RUST_SRC_PATH = "${rustToolchain}/lib/rustlib/src/rust/library";
        LD_LIBRARY_PATH = "${lib.makeLibraryPath buildInputs}";
      };
    });
}
