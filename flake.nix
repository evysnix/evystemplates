{
  description = "A very basic flake";

  outputs = { self }: {
    rust-stable = {
      path = ./rust/stable;
    };
    rust-nightly = {
      path = ./rust/nightly;
    };
    cpp = {
      path = ./cpp;
    };
  };
}
